import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial_example/bt.dart';

void main() => runApp(new ExampleApplication());

class ExampleApplication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: BT());
  }
}
