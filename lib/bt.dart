import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class BT extends StatefulWidget {
  BT({Key key}) : super(key: key);

  @override
  _BTState createState() => _BTState();
}

class _BTState extends State<BT> {
  String _address =
      '60:F2:62:40:C2:1F'; //ENTER MAC ADDRESS FOR DEVICE TO CONNECT TOO HERE...
  BluetoothConnection connection;
  bool isConnecting = true;
  bool isDisconnecting = false;

  @override
  void initState() {
    super.initState();
    _connectBT(_address);
  }

  void _sendMessage(String text) async {
    text = text.trim();
    print(text);

    if (text.length > 0) {
      try {
        connection.output.add(utf8.encode(text + "\r\n"));
        await connection.output.allSent;
      } catch (e) {
        print(e);
        setState(() {});
      }
    }
  }

  void _connectBT(_address) async {
    BluetoothConnection.toAddress(_address).then((_connection) {
      print('Connected to the device');
      connection = _connection;
      setState(() {
        isConnecting = false;
        isDisconnecting = false;
      });

      connection.input.listen(_onDataReceived).onDone(() {
        print('asdasd');
        if (isDisconnecting) {
          print('Disconnecting locally!');
        } else {
          print('Disconnected remotely!');
        }
        if (this.mounted) {
          setState(() {});
        }
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      print(error);
    });
  }

  void _onDataReceived(Uint8List data) {
    int backspacesCounter = 0;
    data.forEach((byte) {
      if (byte == 8 || byte == 127) {
        backspacesCounter++;
      }
    });
    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    int bufferIndex = buffer.length;

    backspacesCounter = 0;
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i] == 8 || data[i] == 127) {
        backspacesCounter++;
      } else {
        if (backspacesCounter > 0) {
          backspacesCounter--;
        } else {
          buffer[--bufferIndex] = data[i];
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_address),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () => _sendMessage('OPEN'),
              child: Text('OPEN'),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 50),
            ),
            GestureDetector(
              onTap: () => _sendMessage('CLOSE'),
              child: Text('CLOSE'),
            ),
          ],
        ),
      ),
    );
  }
}
